import simplejson
import sys
import MySQLdb
from settings import *

file = open('applications.json', 'r')
content = file.read()
json_dict = simplejson.loads(content)
json_list = json_dict['data']

print('file was read')


def trim_string(string):
    """Trim string for insertion"""
    for character in ['\'', ' ', '[', ']']:
        if character in string:
            string = string.replace(character, ' ')
    return string.strip('\'', ).strip(' ').strip(']').strip('[')


# Checking arguments
if len(sys.argv) < 2:
    print('Please provide appropriate arguments')
    print('python setup.py [mode=UPDATE,CREATE]')
    exit()

# Preparing database
db = MySQLdb.connect(DATABASE['HOST'], DATABASE['USER'], DATABASE['PASSWORD'], DATABASE['DATABASE'])
cursor = db.cursor()
# print('Connected to database')

MODE = sys.argv[3]


# If mode is CREATE
if MODE == 'CREATE':
    print('Starting in CREATE mode')

    cursor.execute(
        "CREATE TABLE IF NOT EXISTS codes (id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT, package_name VARCHAR(255) NOT NULL, application_name VARCHAR(255) NOT NULL, shamed_code VARCHAR(255) NOT NULL)")
    print('Created table')
    cursor.execute("CREATE INDEX PACKAGE_NAME_INDEX ON codes(package_name);")
    print('Created index')

print('Starting Iterating over Shamad_json')
for obj in json_list:
    package_name = trim_string(obj["package"])
    latin_name = trim_string(obj["latinname"])
    shamed_code = trim_string(obj["ShamadCode"])
    print(package_name)
    cursor.execute("INSERT INTO codes (package_name, application_name, shamed_code) values ('{}', '{}', '{}')".format(
        package_name,
        latin_name,
        shamed_code)
    )
    print('Package name: {} inserted'.format(package_name))

print('Committing and closing database access')
db.commit()
db.close()
print('Done')
